public class Main {


    public static void main(String[] args) {
        int A[] = {1, 4, 6, 7, 9};
        int B[] = {4, 8, 9};

        int n = A.length;
        int m = B.length;

        System.out.println("Maximum Subarray Sum: " + maxSubArraySum(A, n, B, m));
    }

    public static boolean contain(int[] arr, int m, int value) {
        for (int item : arr) {
            if (item == value)
                return true;
        }
        return false;
    }

    static int maxSubArraySum(int a[], int n, int[] b, int m) {
        int max_so_far = Integer.MIN_VALUE, max_ending_here = 0;
        for (int i = 0; i < n; i++) {
            max_ending_here = max_ending_here + a[i];
            if (contain(b, m, a[i]))
                max_ending_here = 0;
            if (max_ending_here < 0)
                max_ending_here = 0;
            if (max_so_far < max_ending_here)
                max_so_far = max_ending_here;
        }
        return max_so_far;
    }
}
